﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_2_massiivid
{
    class Program
    {
        static void Main(string[] args)
        {
            // ANDMETÜÜPIDE MUUTMINE
            /*
            int s = 7;
            string seitse = s.ToString();
            Console.WriteLine(seitse);

            int s2 = 7;
            string seitse2 = (s2 * s2).ToString();            //1. ToString - igal klassil ehk andmetüübil oma
            Console.WriteLine(seitse2);

            Console.WriteLine("Kui vana sa oled?");
            int vanus = int.Parse(Console.ReadLine());      // 2. igas klassis, mis oskab - oma, nt int.Parse

            //if(int.tryParse(Console.ReadLine(), out int vanus);      

            int pool = (int)(vanus / 2.0);                   //3. casting - ainult arvude puhul, pikemat arvu saab castida lühemaks
            Console.WriteLine(pool);

            int i = 7;
            byte b = (byte)i;
            Console.WriteLine(b);

            int ii = 777;
            byte b2 = (byte)ii;
            Console.WriteLine(b2);


            int a = 1734;
            //short ss = short(ii);

            //int b = Convert.ToInt32();                     //4. Convert - omistamine
            */

            // IF LAUSED
            /*Console.Write("Kui palju sa palka saad? ");
            int palk = int.Parse(Console.ReadLine());

            if (palk > 1000) // semikoolonit ei pane. Võib ka loogeliste sulgudeta panna, kui üks lause, nt Console.WriteLine("Hea palk");
                {
                    Console.WriteLine("Hea palk");
                } 
            else if (palk > 500)
                {
                    Console.WriteLine("Rahuldav palk");
                }
            else if (palk > 200)
                {
                Console.WriteLine("Kehv palk");
                }
            else
                {
                    Console.WriteLine("Väga kehv palk");
                }
            */

            // SWITCH LAUSED
            /*switch(DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                    Console.WriteLine("Täna puhkame");
                    break;
                case DayOfWeek.Wednesday:
                    Console.WriteLine("Täna teeme trenni");
                    goto default;
                case DayOfWeek.Monday:
                case DayOfWeek.Sunday:
                    Console.WriteLine("Täna teeme pannkooki");
                    goto case DayOfWeek.Wednesday;
                case DayOfWeek.Friday:
                    Console.WriteLine("Täna teeme trenni");
                    break;
                default:
                    Console.WriteLine("Täna töötame");
                    break;
            }
            */

            // TINGIMUSLIK ARVUTUSJADA
            /*Console.Write("Kui palju sa palka saad? ");
            int palk = int.Parse(Console.ReadLine());
            string palgasuurus =
                (palk > 1000) ? "Hea palk" :
                (palk > 500) ? "Keskmine palk" :
                (palk > 200) ? "Kehv palk" :
                "Väga kehv palk";
            Console.WriteLine(palgasuurus);
            */


            //ÜLESANNE1
            /*Console.WriteLine("Sisesta nr: ");
            int arv = int.Parse(Console.ReadLine());
            Console.WriteLine("Arv on {0}", arv);
            if(arv % 2 == 0)
            {
                Console.WriteLine("Paarisarv");
            }
            else
            {
                Console.WriteLine("Paaritu arv");
            }
            

            Console.WriteLine(
                arv % 2 == 0 ? "Paarisarv" : "Paaritu arv");
            */


            //ÜLESANNE1
            /*Console.WriteLine("What colour do you see?  ");
            string c = Console.ReadLine().ToLower();
            switch (c)
            {
                case "GREEN":
                case "green":
                    Console.WriteLine("Driver can drive a car");
                    break;
                case "YELLOW":
                case "yellow":
                    Console.WriteLine("Driver has to be ready to stop");
                    break;
                case "RED":
                case "red":
                    Console.WriteLine("Driver has to stop");
                    break;
                default:
                    Console.WriteLine("The color is unknown.");
                    break;
            }
            */


            /*
            int arv = 7;   // muutuja int, teda on üks ja tal on väärtus
            int[] arvud1 = new int[10];    // muutuja int'ide mass ja neid on kokku 10

            int[] arvud2 = { 1, 2, 3, 4, 5 * 5, 6, 7 }; // või
            int[] arvud3 = new int[7]  { 1, 2 + arv, 3, 4, 5 * 5, 6, 7 };
            Console.WriteLine(arvud3 [5]);

            string[] mastid = { "Poti", "Ärtu", "Ruutu", "Risti" };
            Console.WriteLine(mastid[0]);
            */

            /*
            Console.WriteLine("Mis su nimi on? ");
            String[] nimed = Console.ReadLine().Split(' ');
            Console.WriteLine("Sinu eesnimi on {0}", nimed[0]);
            if (nimed.Length > 1)
            {
                Console.WriteLine("Sinu perenimi on {0}", nimed[1]);
            }
            */

            /*
            // TSÜKLID
            int[] mass = new int[10];
            
            // FOR TSÜKKEL
            for (int i = 0; i < mass.Length; i++)
            {
                mass[i] = i * i;
                Console.WriteLine(mass[i]);
            }


            //FOREACH TSÜKKEL
            foreach (int x in mass)
            {
                Console.WriteLine(x);
            }

            // WHILE TSÜKKEL
            int y = 0;
            while (mass[y++] < 5)
            {
                Console.WriteLine(y);
            }

            // DO TSÜKKEL
            int z = 0;
            do
            {
                Console.WriteLine(z);
            } while (mass[z++] < 5);
            */


            string nimi = "Mati";
            int vanus = 30;

            string vastus = nimi + " on " + vanus.ToString() + " aastat vana"; //või
            //string vastus = $"{nimi} on varsti {vanus + 1} aastat vana");
            Console.WriteLine(vastus);

            string vastus2 = string.Format("{0} on {1} aastat vana", nimi, vanus);
            Console.WriteLine(vastus2);

            Console.WriteLine("{0} on {1} aastat vana", nimi, vanus);

            Console.WriteLine($"{nimi} on varsti {vanus+1} aastat vana");

        }
    }
}
